package kz.aitu.SumCode;

public class Main {
    public static void main(String[] args) {
        int[] arr={1, 4, 45, 6, 10, -8};
        int findSum=16;
        int[] Overal = slow(arr,16);
        print(Overal);
    }

    static void bubbleSort(int[] arr) {
        int size = arr.length;
        int temp = 0;
        for(int i=0; i < size; i++){
            for(int j=1; j < (size-i); j++){
                if(arr[j-1] > arr[j]){
                    temp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }

            }
        }

    }

    static int[] slow (int[]arr, int findSum){
        int[] theSumOf={0,0};
        int forward=0;
        int backward = arr.length-1;
        while (forward<backward){
            if(arr[forward] + arr[backward] == findSum){
                theSumOf[0]=arr[forward];
                theSumOf[1]=arr[backward];
            }
            if(arr[forward] + arr[backward] < findSum){
                forward++;
            }
            if(arr[forward] + arr[backward] > findSum){
                backward--;
            }
        }
        return theSumOf;
    }

    static void print(int[]arr){
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
    }
}
