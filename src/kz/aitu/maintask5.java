package task5;

import java.util.Scanner;

public class maintask5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int[]arr = new int[num];
        for(int i=0;i<num;i++) {
            int num2 = scanner.nextInt();
            arr[i] = num2;
        }
        sort(arr);
        print(arr, num);
        System.out.println(counterSame(arr));
    }

    static void print(int[]arr, int num){
        for(int i=0;i<num;i++) {
            System.out.print(arr[i]+" ");
        }
    }

    static int counterSame(int[] arr){
        int counter=0;
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        for(int i=0;i<arr.length;i++){
            if(arr[i]==num){
                counter++;
            }
        }
        return counter;
    }

    static void sort(int[] arr){
        for(int i=0;i<arr.length;i++){
            for(int j=i+1;j<arr.length-i;j++){
                if(arr[j-1]>arr[j]){
                    int temp = arr[j-1];
                    arr[j-1]=arr[j];
                    arr[j]=temp;
                }
            }
        }
    }
}
