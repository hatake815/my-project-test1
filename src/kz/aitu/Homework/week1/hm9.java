package kz.aitu.Homework.week1;

import java.util.Scanner;

public class hm9 {
    public static int binomial(int num){
        if(num<=1){
            return 1;
        }
        else {
            return num*binomial(num-1);
        }
    }

    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        int num1=input.nextInt();
        int num2=input.nextInt();
        long a=binomial(num1)/(binomial(num2)*binomial(num1-num2));
        System.out.println(a);
    }
}
