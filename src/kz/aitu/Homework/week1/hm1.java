package kz.aitu.Homework.week1;

import java.util.Scanner;

public class hm1 {
        public static int getSmallest(int[] a, int total){
            int temp;
            for (int i = 0; i < total; i++)
            {
                for (int j = i + 1; j < total; j++)
                {
                    if (a[i] > a[j])
                    {
                        temp = a[i];
                        a[i] = a[j];
                        a[j] = temp;
                    }
                }
            }
            return a[0];
        }

        public static void main(String[] args) {
            int n;
            Scanner input = new Scanner(System.in);
            n = input.nextInt();
            int arr[]= new int[n];
            for(int i=0;i<n;i++){
                arr[i]=input.nextInt();
            }
            System.out.println(getSmallest(arr,n));
        }
    }
