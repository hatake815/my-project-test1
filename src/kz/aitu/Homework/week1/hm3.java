package kz.aitu.Homework.week1;

import java.util.Scanner;

public class hm3 {
        public static int numberChecker (int number){
            int sum=0;
            for(int i=1;i<=number;i++){
                if(number%i==0){
                    sum++;
                }
            }
            return sum;
        }

        public static void main(String[] args) {
            Scanner input = new Scanner(System.in);
            int n = input.nextInt();
            int sum = numberChecker(n);
            if(sum==2){
                System.out.println("Prime");
            } else{
                System.out.println("Composite");
            }
        }
}
