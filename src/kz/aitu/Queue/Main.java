package kz.aitu.Queue;

public class Main {

    public static void main(String[] args) {
        Group group = new Group();
        group.add(new Block(12));
        group.add(new Block(2));
        group.add(new Block(22));
        System.out.println(group.size());
        group.remove();
        System.out.println(group.size());
        System.out.println(group.poll());
        System.out.println(group.size());
        group.remove();
        System.out.println(group.size());
    }
}
