package kz.aitu.Queue;

public class Block {
    private int data;
    private Block next;

    public Block(int data){
        this.data=data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setNext(Block next) {
        this.next = next;
    }

    public Block getNext() {
        return next;
    }

    public int getData() {
        return data;
    }
}
