package kz.aitu.Queue;

public class Group {
    int counter=0;
    private Block head;
    private Block tail;

    public void setHead(Block head) {
        this.head = head;
    }

    public void setTail(Block tail) {
        this.tail = tail;
    }

    public Block getHead() {
        return head;
    }

    public Block getTail() {
        return tail;
    }

    public void add(Block block){
        if(head==null){
            head=block;
            tail=block;
        }
        else {
          tail.setNext(block);
          tail=block;
        }
        counter++;
    }

    public Block peek(){
        if(head==null){
            return null;
        }
        else{
            return head;
        }
    }

    public Block remove(){
        head=head.getNext();
        counter--;
        return head;
    }

    public Block poll(){
        if(head==null){
            return null;
        }
        else {
            head=head.getNext();
            counter--;
            return head;
        }
    }

    public int size(){
        return counter;
    }
}
