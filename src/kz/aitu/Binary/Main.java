package kz.aitu.Binary;

import java.util.Scanner;

class Main{

    static class Node{

        private int data;
        private Node left;
        private Node right;

        public Node(int data)
        {
            this.data = data;
            left = null;
            right = null;
        }
    }

    static boolean exists(Node node, int key)
    {
        if (node == null){
            return false;
        }

        if (node.data == key){
            return true;
        }

        boolean searchNextLeft = exists(node.left, key);
        if(searchNextLeft) {
            return true;
        }

        boolean searchNextRight = exists(node.right, key);
        return searchNextRight;
    }

    public static void main(String args[])
    {
        Node root = new Node(0);
        root.left=new Node(1);
        root.left.left=new Node(3);
        root.left.left.left=new Node(7);
        root.left.right=new Node(4);
        root.left.right.left=new Node(8);
        root.left.right.right=new Node(9);
        root.right=new Node(2);
        root.right.left=new Node(5);
        root.right.right=new Node(6);

        Scanner scanner=new Scanner(System.in);
        int number= scanner.nextInt();

        if(exists(root, number)){
            System.out.println("YES");
        }
        else {
            System.out.println("NO");
        }
    }
}

