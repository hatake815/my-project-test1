package kz.aitu.HashTable;

public class Node {
    private String key;
    private String data;
    private Node next;

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setValue(String data) {
        this.data = data;
    }

    public String getValue() {
        return data;
    }

    public Node(String key, String data){
        this.key = key;
        this.data = data;
        next = null;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext() {
        return next;
    }

}
