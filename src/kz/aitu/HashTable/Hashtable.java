package kz.aitu.HashTable;

public class Hashtable {
    private Node[] table;
    int size;

    public Hashtable(int tablesize){
        table = new Node[tablesize];
        size = tablesize;
    }

    public void insert(String key,String value) {
        Node newCurrent = new Node(key, value);
        if (table[key.hashCode() % size] == null) {
            table[key.hashCode() % size] = newCurrent;
        } else {
            Node current = table[key.hashCode() % size];
            while (current!=null){
                if (current.getKey() == newCurrent.getKey()){
                    current.setValue(newCurrent.getValue());
                    break;
                }
                current = current.getNext();
            }
            if (current == null){
                newCurrent.setNext(table[key.hashCode() % size]);
                table[key.hashCode() % size] = newCurrent;
            }

        }
    }

    public void print(){
        for (int i =0;i<size;i++){
            Node current = table[i];
            while (current!=null){
                System.out.print(current.getValue()+" ");
                current = current.getNext();
            }
            System.out.println("");
        }
    }

    public String get(String key){
        Node current = table[key.hashCode()%size];
        while (current!=null){
            if(current.getKey()==key){
                return current.getValue();
            }
            current = current.getNext();
        }
        System.out.println("No key such as: "+key);
        return key;
    }

    public void remove(String key){
        Node current = table[key.hashCode()%size];
        Node prev = null;
        if (current.getKey()==key){
            table[key.hashCode()%size]=null;
        } else {
            while (current != null) {
                if (current.getKey() == key) {
                    prev.setNext(current.getNext());
                }
                prev = current;
                current = current.getNext();
            }
        }
    }
}
