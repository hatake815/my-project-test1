package kz.aitu.week4;

public class Node {
    private String data;
    private Node next;
    //добавить элементы Node


    public Node getNext() {
        return next;
    }
    //get next

    public String getData() {
        return data;
    }
    //get data
    
    public void setData(String data) {
        this.data = data;
    }//set data

    public void setNext(Node next) {
        this.next = next;
    }//set next
}
