package kz.aitu.HackerRank;

public class all {
}
/*

Print the Elements of a Linked List:

 static void printLinkedList(SinglyLinkedListNode head) {
        SinglyLinkedListNode currentNode = head;

        while(currentNode != null){
            System.out.println(currentNode.data);
            currentNode = currentNode.next;
        }
    }


Insert a node at the head of a linked list:

  static SinglyLinkedListNode insertNodeAtHead(SinglyLinkedListNode head, int data) {
            SinglyLinkedListNode temp = new SinglyLinkedListNode(data);
            if(head==null)
                return temp;
            temp.next=head;
            head=temp;
            return head;

    }

    Insert a Node at the Tail of a Linked List:

    static SinglyLinkedListNode insertNodeAtTail(SinglyLinkedListNode head, int data) {
        SinglyLinkedListNode newNode = new SinglyLinkedListNode(data);
        if(head == null){
            head = newNode;
            return head;
        }
        SinglyLinkedListNode auxNode = head;
        while(auxNode.next != null){
            auxNode = auxNode.next;
        }
        auxNode.next = newNode;
        return head;
    }


Insert a node at a specific position in a linked list:

    static SinglyLinkedListNode insertNodeAtPosition(SinglyLinkedListNode head, int data, int position) {
        SinglyLinkedListNode insert = new SinglyLinkedListNode(data);
        SinglyLinkedListNode rs = head;
        SinglyLinkedListNode temp = head;
        for(int i = 0; i < position - 1; i++){
            temp = temp.next;
        }
        SinglyLinkedListNode next = temp.next;
        temp.next = insert;
        insert.next = next;
        return rs;
}

Delete a Node:

   static SinglyLinkedListNode deleteNode(SinglyLinkedListNode head, int position) {
        SinglyLinkedListNode node = head;
        if( position == 0 )
        {
          head = head.next;
        }
        else
        {
          for( int i=1;i<position; i++)
            node = node.next;
          node.next = node.next.next;
        }

        return head;

    }

Compare two linked lists:

 static boolean compareLists(SinglyLinkedListNode head1, SinglyLinkedListNode head2) {
        SinglyLinkedListNode p1=head1;
        SinglyLinkedListNode p2=head2;
        while(p1!=null && p2!=null){
            if (p1.data!=p2.data)
                return false;
            p1=p1.next;
            p2=p2.next;
        }
        if (p1!=null || p2!=null)
            return false;
        return true;
    }

    Get Node Value:

       static int getNode(SinglyLinkedListNode head, int positionFromTail) {
        SinglyLinkedListNode a = head;
        SinglyLinkedListNode b = head;
        while(positionFromTail-- > 0) {
            b = b.next;
        }
        while(b.next != null) {
            a = a.next;
            b = b.next;
        }
        return a.data;
    }

    Delete duplicate-value nodes from a sorted linked list:


     static SinglyLinkedListNode removeDuplicates(SinglyLinkedListNode head) {
        SinglyLinkedList node = new SinglyLinkedList();
        int a = 0;
        while(head != null){
            if(head.data > a){
                node.insertNode(head.data);
                a = head.data;
            }
            head = head.next;
        }

        return node.head;
    }


 */