package kz.aitu.LinkedList.Size;

import java.util.Scanner;

public class Main {
    public static int printGroup(Group group) {
        Block block = group.getFirst();
        int y = 0;
        while(block != null) {
            System.out.print(block.getValue() + " ");
            block = block.getNextBlock();
            y++;
        }
        System.out.println();
        return y;
    }

    public static void main(String[] args) {
        Group group1 = new Group();
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many?");
        int number =scanner.nextInt();
        System.out.println("Enter your"+" "+number+" "+"numbers!");
        for(int i=0;i<number;i++){
            int n=scanner.nextInt();
            Block a = new Block();
            a.setValue(n);
            a.setNextBlock(null);
            group1.addBlock(a);
        }

        System.out.println(printGroup(group1));
    }
}
