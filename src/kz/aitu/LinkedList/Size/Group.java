package kz.aitu.LinkedList.Size;

public class Group {
    private Block first;
    private Block last;

    public Block getFirst() {
        return first;
    }

    public Block getLast() {
        return last;
    }

    public void setFirst(Block first) {
        this.first = first;
    }

    public void setLast(Block last) {
        this.last = last;
    }

    public void addBlock(Block block){
        if (first==null){
            first = block;
            last = block;
        } else {
            last.setNextBlock(block);
            last = block;
        }
    }
}
