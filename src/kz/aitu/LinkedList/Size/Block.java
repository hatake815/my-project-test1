package kz.aitu.LinkedList.Size;

public class Block {
    private int value;
    private Block nextBlock;

    public Block getNextBlock() {
        return nextBlock;
    }

    public int getValue() {
        return value;
    }

    public void setNextBlock(Block nextBlock) {
        this.nextBlock = nextBlock;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
