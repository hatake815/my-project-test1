package kz.aitu.LinkedList.Reversed;

public class Node {
    private Block head;
    private Block tail;

    public void setTail(Block tail) {
        this.tail = tail;
    }

    public void setHead(Block head) {
        this.head = head;
    }

    public Block getHead() {
        return head;
    }

    public Block getTail() {
        return tail;
    }

    public void addBlock(Block block) {
        if(head==null){
            head=block;
            tail=block;
        }
        else {
            tail.setNext(block);
            tail=block;
        }
    }

    public void print(Node node){
        while (head!=null){
            System.out.println(node.getHead()+" ");
        }
        System.out.println();
    }
}
