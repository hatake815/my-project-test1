package kz.aitu.LinkedList.Reversed;

public class Block {
    private int data;
    private Block next;

    public void setNext(Block next) {
        this.next = next;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Block getNext() {
        return next;
    }

    public int getData() {
        return data;
    }

    public Block (int data){
        this.data=data;
    }

    public void getBlock(Node node){
        while (node.getHead()==null){
            System.out.println(data+" ");
        }
    }
}
