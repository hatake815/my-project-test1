package kz.aitu.Sort;

public class Bubble {
    static void bubbleSort(int[] arr) {
        int size = arr.length;
        int temp = 0;
        for(int i=0; i < size; i++){
            for(int j=1; j < (size-i); j++){
                if(arr[j-1] > arr[j]){
                    temp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }

            }
        }

    }

    static void printArray(int[]arr){
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
    }

    public static void main(String[] args) {
        int arr[] ={3,60,35,2,45,320,5};
        bubbleSort(arr);
        printArray(arr);
    }
}
