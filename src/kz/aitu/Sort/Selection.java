package kz.aitu.Sort;

public class Selection {
    public static void main(String[] args) {

        int[] array = {3,60,35,2,45,320,59};
        selectionSort(array);
        printArray(array);
    }

     static void selectionSort(int[] input) {
        int length = input.length;
        for (int i = 0; i < length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < length; j++) {
                if (input[j] < input[min]) {
                    min = j;
                }
            }
            int temp = input[i];
            input[i] = input[min];
            input[min] = temp;
        }

    }

    static void printArray(int arr[])
    {
        int n = arr.length;
        for (int i=0; i<n; ++i) {
            System.out.print(arr[i] + " ");
        }
    }
}
