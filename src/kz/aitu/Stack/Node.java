package kz.aitu.Stack;

public class Node {
    private int Value;
    private Node next;

    public Node(int value){
        this.Value=value;
    }

    public Node next(){return next;}
    public int getValue(){return Value;}

    public void setValue(int value) {
        this.Value = value;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}