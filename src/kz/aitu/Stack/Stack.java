package kz.aitu.Stack;

import java.util.Arrays;
import java.util.Scanner;

public class Stack {
    private int size=0;
    private Node top;

    public Node getTop(){
        return top;
    }

    public void push(Node node) {
        node.setNext(top);
        top=node;
        size++;
    }

    public void pop(){
    top=top.next();
    size--;
    }

    public boolean empty(){
        if(top==null){
            return true;
        }
        else {
           return false;
        }
    }

    public int sizeN(){
        int i=0;
        while(top!=null){
            i++;
            top=top.next();
        }
        return i;
    }

    public int size(){
        return size;
    }

   public Stack combine (Stack first, Stack second, Stack third){
        Stack all = new Stack();
        Stack current1=first;
        Stack current2=second;
        Stack current3=third;

       if(current1==null || current2==null || current3==null){
            return all;
        }
          else {
           all.compare(current1,current2,current3);
           first.pop();
           second.pop();
           third.pop();
           return combine(current1,current2,current3);
       }
   }

   public Stack compare(Stack first, Stack second, Stack third){
       Stack all = new Stack();
       int one = first.top.getValue();
       int two = second.top.getValue();
       int three = third.top.getValue();
       int [] arr = {one,two,three};
       Arrays.sort(arr);
       if(one==two && two==three){
           all.push(first.top);
       }
       if(one==two && two!=three){
           all.push(first.top);
           all.push(second.top);
       }
       if(two==three && one!=three){
           all.push(second.top);
           all.push(first.top);
       }
       if(one==arr[0] && two==arr[1] && three==arr[2]){
           all.push(first.top);
           all.push(second.top);
           all.push(third.top);
       }
       if(one==arr[0] && two==arr[2] && three==arr[1]){
           all.push(first.top);
           all.push(third.top);
           all.push(second.top);
       }
       if(one==arr[1] && two==arr[0] && three==arr[2]){
           all.push(second.top);
           all.push(first.top);
           all.push(third.top);
       }
       if(one==arr[2] && two==arr[0] && three==arr[1]){
           all.push(second.top);
           all.push(third.top);
           all.push(first.top);
       }
       if(one==arr[2] && two==arr[1] && three==arr[0]){
           all.push(third.top);
           all.push(second.top);
           all.push(first.top);
       }
       if(one==arr[1] && two==arr[2] && three==arr[0]){
           all.push(third.top);
           all.push(first.top);
           all.push(second.top);
       }
       return all;
   }
   public void pushMany(Stack stack){
       Scanner scanner = new Scanner(System.in);
       String stop = scanner.nextLine();
       while (stop!="n"){
           int number = scanner.nextInt();
        stack.push(new Node(number));
       }
   }
}

//1 2 3