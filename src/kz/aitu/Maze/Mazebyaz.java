package kz.aitu.Maze;

import java.util.Scanner;

public class Mazebyaz {
    int a[][] = new int[5][5];
    public void inputData() {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i ++) {
            for (int j = 0; j < 5; j ++) {
                a[i][j] = scanner.nextInt();
            }
        }
    }

    public void print() {
        for (int i = 0; i < 5; i ++) {
            for (int j = 0; j < 5; j ++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    public boolean findPath(int i, int j) {
        if (a[4][4]==1||a[0][0]==1){
            return true;
        }
        if(a[4][4]==0||a[0][0]==0){
            return false;
        }
        if (a[i - 1][j] == 1 || a[i + 1][j] == 1 || a[i][j - 1] == 1 || a[i][j + 1] == 1) {
            findPath(i,j);
        }
        System.out.println(a[i][j]==2);
        return findPath(i,j);
    }



    public void run() {
        inputData();
            System.out.println(findPath(0,0));
        print();
    }
    public static void main(String[] args){
        Mazebyaz m = new Mazebyaz();
        m.run();
    }
}
