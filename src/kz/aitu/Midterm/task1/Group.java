package kz.aitu.Midterm.task1;

public class Group {
    private Block head;
    private Block tail;

    public void setTail(Block tail) {
        this.tail = tail;
    }

    public void setHead(Block head) {
        this.head = head;
    }

    public Block getHead() {
        return head;
    }

    public Block getTail() {
        return tail;
    }

    public int print() {
        System.out.println(head.getData());
        head = head.getNext();

        if (head == null) {
            return 1;
        }
        return print();
    }
}
