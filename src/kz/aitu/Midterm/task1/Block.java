package kz.aitu.Midterm.task1;

public class Block {
    private int data;
    private Block next;

    public void setNext(Block next) {
        this.next = next;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Block getNext() {
        return next;
    }

    public int getData() {
        return data;
    }
}
