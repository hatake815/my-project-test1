package kz.aitu.Midterm.task3;

public class Block {
    private String data;
    private Block next;

    public Block(String data){
        this.data=data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setNext(Block next) {
        this.next = next;
    }

    public Block getNext() {
        return next;
    }

    public String getData() {
        return data;
    }
}
