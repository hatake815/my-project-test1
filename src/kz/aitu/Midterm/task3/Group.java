package kz.aitu.Midterm.task3;


public class Group {
    private Block head;
    private Block tail;

    public void setHead(Block head) {
        this.head = head;
    }

    public void setTail(Block tail) {
        this.tail = tail;
    }

    public Block getHead() {
        return head;
    }

    public Block getTail() {
        return tail;
    }

    public void push(Block block){
        if(head==null){
            head=block;
            tail=block;
        }
        else {
          tail.setNext(block);
          tail=block;
        }
    }

    public Block pop(){
        head=head.getNext();
        return head;
    }

    public int printGroup(Group group) {//O(n)
        Block block = group.getHead();
        while (block != null) {
            if(block.getData().length()%2!=0){
                System.out.print(block.getData() + " ");
            }
            block = block.getNext();
        }
        System.out.println();
        return 0;
    }
}
