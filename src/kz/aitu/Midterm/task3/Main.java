package kz.aitu.Midterm.task3;

public class Main {

    public static void main(String[] args) {
        Group group = new Group();
        group.push(new Block("Hello"));
        group.push(new Block("this"));
        group.push(new Block("is"));
        group.push(new Block("my"));
        group.push(new Block("Programm!"));
        group.pop();
        group.printGroup(group);
    }
}
