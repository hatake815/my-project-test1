package kz.aitu.Test;

public class Group {
    private Block head;
    private Block tail;

    public void setHead(Block head) {
        this.head = head;
    }

    public void setTail(Block tail) {
        this.tail = tail;
    }

    public Block getTail() {
        return tail;
    }

    public Block getHead() {
        return head;
    }

    public void addBlock(Block block){
        if(head==null){
            head=block;
            tail=block;
        }
        else{
            while (head!=null){
                head.setNext(block);
            }
        }
    }

    public int printGroup(){
        Block block =new Block();
        while (head!=null){
            return block.getData();
        }
        return block.getData();
    }
}
