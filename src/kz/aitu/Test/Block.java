package kz.aitu.Test;

public class Block {
    private int data;
    private Block next;

    public void setData(int data) {
        this.data = data;
    }

    public void setNext(Block next) {
        this.next = next;
    }

    public int getData() {
        return data;
    }

    public Block getNext() {
        return next;
    }
}
